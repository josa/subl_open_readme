import sublime, sublime_plugin, glob, re, os

class OpenReadmeCommand(sublime_plugin.WindowCommand):

    def run(self):

        self.readmes = []

        for file in glob.glob(os.path.join(sublime.packages_path(), '*/*')):
            m = re.search('.*/([^\/]*)/readme[^\/]*$', file, flags=re.IGNORECASE)
            if m:
                self.readmes.append([m.group(1), m.group(0)])

        self.window.show_quick_panel(self.readmes, self.onSelect)

    def onSelect(self, i):
        if (i >= -1):
            view = self.window.open_file(self.readmes[i][1])
            view.set_read_only(True)
